import numpy as np
from multiagent.core import World, Agent, Landmark, Obstacle
from multiagent.scenario import BaseScenario


class Scenario(BaseScenario):
    def make_world(self):
        world = World()
        # set any world properties first
        world.dim_c = 2
        num_agents = 3
        num_landmarks = 3
        num_obstacles = 2

        # add agents
        world.agents = [Agent() for i in range(num_agents)]
        for i, agent in enumerate(world.agents):
            agent.name = 'agent %d' % i
            agent.collide = True
            agent.silent = True
            agent.size = 0.05

        # add landmarks
        world.landmarks = [Landmark() for i in range(num_landmarks)]
        for i, landmark in enumerate(world.landmarks):
            landmark.name = 'landmark %d' % i
            landmark.collide = False
            landmark.movable = False
            landmark.size = 0.05

        # add obstacles
        world.obstacles = [Obstacle() for i in range(num_obstacles)]
        for i, obstacle in enumerate(world.obstacles):
            obstacle.name = 'obstacle %d' % i
            obstacle.collide = False
            obstacle.movable = False
            #obstacle.size = 0.3
        # make initial conditions
        self.reset_world(world)
        return world

    def reset_world(self, world):
        # random properties for agents
        world.agents[0].color = np.array([1.0, 0.0, 0.0])
        world.agents[1].color = np.array([0.0, 1.0, 0.0])
        world.agents[2].color = np.array([0.0, 0.0, 1.0])
        # setcolor for landmarks
        for i, landmark in enumerate(world.landmarks):
            landmark.color = np.array([0.25, 0.25, 0.25])
        # setcolor for obstacles
        for i, obstacle in enumerate(world.obstacles):
            obstacle.color = np.array([1.0, 1.0, 0.0])

        #障害物の位置決定
        for i in range(len(world.obstacles)):
            world.obstacles[i].size_update(np.random.uniform(+0.3, +0.6, 2))
            world.obstacles[i].state.p_pos = np.random.uniform(-1, +1, world.dim_p)
            world.obstacles[i].state.p_vel = np.zeros(world.dim_p)

        for i in range(len(world.landmarks)):
            while(True):
                pos_flag = True
                world.landmarks[i].state.p_pos = np.random.uniform(-1, +1, world.dim_p)
                for j in range(len(world.obstacles)):
                    if(self.is_collision2(world.landmarks[i],world.obstacles[j])):
                        pos_flag = False
                for j in range(i):
                    if(np.sqrt(np.sum(np.square(world.landmarks[i].state.p_pos - world.landmarks[j].state.p_pos))) < 0.2):
                        pos_flag = False
                if(pos_flag == True):
                    break
            world.landmarks[i].state.p_vel = np.zeros(world.dim_p)
        
        for i in range(len(world.agents)):
            while(True):
                pos_flag = True
                world.agents[i].state.p_pos = np.random.uniform(-1, +1, world.dim_p)
                for j in range(len(world.obstacles)):
                    if(self.is_collision2(world.agents[i],world.obstacles[j])):
                        pos_flag = False
                for j in range(len(world.landmarks)):
                    if(self.is_collision(world.agents[i],world.landmarks[j])):
                        pos_flag = False
                for j in range(i):
                    if(np.sqrt(np.sum(np.square(world.agents[i].state.p_pos - world.agents[j].state.p_pos))) < 0.2):
                        pos_flag = False
                if(pos_flag == True):
                    break

            world.agents[i].state.p_vel = np.zeros(world.dim_p)
            world.agents[i].state.c = np.zeros(world.dim_c)
            world.agents[i].lider_update()
            #print(world.agents[i].state.p_pos)
        
        
    def benchmark_data(self, agent, world):
        rew = 0
        collisions = 0
        occupied_landmarks = 0
        min_dists = 0
        for l in world.landmarks:
            dists = [np.sqrt(np.sum(np.square(a.state.p_pos - l.state.p_pos))) for a in world.agents]
            min_dists += min(dists)
            rew -= min(dists)
            if min(dists) < 0.1:
                occupied_landmarks += 1
        if agent.collide:
            for a in world.agents:
                if self.is_collision(a, agent):
                    rew -= 1
                    collisions += 1
                    #agent.robot_collision += 1
        for o in world.obstacles:
            if(self.is_collision2(agent,o)):
                rew -=2
                collisions += 1
                #agent.obstacle_collision += 1
        #print(world.robot_collision, world.obstacle_collision)
        return (rew, collisions, min_dists, occupied_landmarks)

    #agent and agent
    def is_collision(self, agent1, agent2):
        delta_pos = agent1.state.p_pos - agent2.state.p_pos
        dist = np.sqrt(np.sum(np.square(delta_pos)))
        dist_min = agent1.size + agent2.size
        return True if dist < dist_min else False

    #agent1->circle agent2->rect
    def is_collision2(self, agent1, agent2):
        R_reg = 2.0
        Xc,Yc = agent1.state.p_pos[0],agent1.state.p_pos[1]
        X1,Y1 = agent2.state.p_pos[0]-(agent2.side/2.0), agent2.state.p_pos[1]+(agent2.ver/2.0)
        X2,Y2 = X1+agent2.side, Y1-agent2.ver
        R = agent1.size*R_reg
        if((Xc+R>X1 and Xc-R<X2) and (Yc-R<Y1 and Yc+R>Y2)):
            return True
        return False

    def reward(self, agent, world):
        # Agents are rewarded based on minimum agent distance to each landmark, penalized for collisions
        rew = 0
        for l in world.landmarks:
            dists = [np.sqrt(np.sum(np.square(a.state.p_pos - l.state.p_pos))) for a in world.agents]
            rew -= min(dists)
            if(min(dists) < 0.05*4):
                agent.reach_goal = 1
            else:
                agent.reach_goal = 0



        if agent.collide:
            for a in world.agents:
                if self.is_collision(a, agent):
                    rew -= 1.0
                    agent.robot_collision = 1
                else:
                    agent.robot_collision = 0

        for o in world.obstacles:
            if(self.is_collision2(agent,o)):
                rew -=2
                agent.obstacle_collision = 1
            else:
                agent.obstacle_collision = 0

        return rew

    def observation(self, agent, world):
        # get positions of all entities in this agent's reference frame
        entity_pos = []
        for entity in world.landmarks:  # world.entities:
            entity_pos.append(entity.state.p_pos - agent.state.p_pos)
        # entity colors
        entity_color = []
        for entity in world.landmarks:  # world.entities:
            entity_color.append(entity.color)
        # communication of all other agents
        comm = []
        other_pos = []
        for other in world.agents:
            if other is agent: continue
            comm.append(other.state.c)
            other_pos.append(other.state.p_pos - agent.state.p_pos)

        lider_dists = []
        for i in range(agent.lider_num):
            dist = 1.0
            for o in world.obstacles:
                for j in range(4):
                    pointC = o.state.p_pos+o.points[j]
                    pointD = []
                    if(j == 3):
                        pointD = o.state.p_pos+o.points[0]
                    else:
                        pointD = o.state.p_pos+o.points[j+1]
                    data = self.calc_cross_point(agent.state.p_pos,agent.lider_lines[i],pointC,pointD)
                    if(data[0] == True):
                        if(dist > data[2]):
                            dist = data[2]
            lider_dists.append(self.map(dist,0.0,1.0,1.0,0.0))
        #obs = np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos + other_pos + comm + [np.array(lider_dists)])
        obs = np.concatenate([agent.state.p_vel] + [agent.state.p_pos] + entity_pos + [np.array(lider_dists)] + other_pos  + comm)
        return obs

    def calc_cross_point(self, pointA, pointB, pointC, pointD):
        cross_point = [0,0]
        bunbo = (pointB[0] - pointA[0]) * (pointD[1] - pointC[1]) - (pointB[1] - pointA[1]) * (pointD[0] - pointC[0])

        # 直線が平行な場合
        if (bunbo == 0):
            return False, cross_point

        vectorAC = ((pointC[0] - pointA[0]), (pointC[1] - pointA[1]))
        r = ((pointD[1] - pointC[1]) * vectorAC[0] - (pointD[0] - pointC[0]) * vectorAC[1]) / bunbo
        s = ((pointB[1] - pointA[1]) * vectorAC[0] - (pointB[0] - pointA[0]) * vectorAC[1]) / bunbo
        if (r <= 0) or (1 <= r) or (s <= 0) or (1 <= s):
            return False, cross_point
        # rを使った計算の場合
        distance = [(pointB[0] - pointA[0]) * r, (pointB[1] - pointA[1]) * r]
        cross_point = [pointA[0] + distance[0], pointA[1] + distance[1]]
        dist = np.sqrt(np.sum(np.square(distance)))
        return True, cross_point, dist

    def map(self, x, in_min, in_max, out_min, out_max):
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min