https://github.com/openai/maddpg


cd ./exp-18-1q-master/exp-18-1q-master/maddpg/experiments
# training
python train.py --scenario simple --num-episode 10000
python train.py --scenario simple_spread_avoid --num-episode 200000

# play using the trained model
python train.py --scenario simple --display --load-model ./experiments/exp_simple_13-03-2018_09-13-02/models/model-59000

python train.py --scenario simple_spread_avoid --display --load-model ./exp_results/simple_spread_avoid_0506/models/model-200000

python train.py --scenario simple_spread_avoid2 --display --load-model ./exp_results/simple_spread_avoid2_051102/models/model-25000

python train.py --scenario simple_spread_avoid2 --display --load-model ./exp_results/simple_spread_avoid_0513/models/model-290000