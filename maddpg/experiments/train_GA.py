# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 15:15:51 2021

@author: mlab
"""
import numpy as np
from chainerNet import Net
from deap import base
from deap import tools
from mydeap import deapNp
import random
import argparse
import os
import os.path as osp
import time
from gym.wrappers.monitoring import video_recorder as gvr
import datetime
dt_now = datetime.datetime.now()

def parse_args():
    parser = argparse.ArgumentParser("Reinforcement Learning experiments for multiagent environments")
    # Environment
    parser.add_argument("--scenario", type=str, default="simple_spread_avoid", help="name of the scenario script")
    parser.add_argument("--max-episode-len", type=int, default=40, help="maximum episode length")#25
    parser.add_argument("--num-episodes", type=int, default=60000, help="number of episodes")
    parser.add_argument("--num-adversaries", type=int, default=0, help="number of adversaries")
    parser.add_argument("--good-policy", type=str, default="maddpg", help="policy for good agents")
    parser.add_argument("--adv-policy", type=str, default="maddpg", help="policy of adversaries")
    # Core training parameters
    parser.add_argument("--lr", type=float, default=1e-2, help="learning rate for Adam optimizer")
    parser.add_argument("--gamma", type=float, default=0.95, help="discount factor")
    parser.add_argument("--batch-size", type=int, default=1024, help="number of episodes to optimize at the same time")#1024
    parser.add_argument("--num-units", type=int, default=64, help="number of units in the mlp")
    # Checkpointing
    parser.add_argument("--exp-name", type=str, default=None, help="name of the experiment")
    parser.add_argument("--save-dir", type=str, default=None, help="directory in which training state and model should be saved")
    parser.add_argument("--save-rate", type=int, default=1000, help="save model once every time this many episodes are completed")
    parser.add_argument("--load-model", type=str, default=None, help="loaded model")
    # Evaluation
    parser.add_argument("--restore", action="store_true", default=False)
    parser.add_argument("--display", action="store_true", default=False)
    parser.add_argument("--benchmark", action="store_true", default=False)
    parser.add_argument("--benchmark-iters", type=int, default=100000, help="number of iterations run for benchmarking")
    parser.add_argument("--benchmark-dir", type=str, default="./benchmark_files/", help="directory where benchmark data is saved")
    parser.add_argument("--plots-dir", type=str, default=None, help="directory where plot data is saved")
    # added by JK
    parser.add_argument("--video-record", action="store_true", default=False, help='if ture, record a video')
    parser.add_argument("--video-file-name", type=str, default=None)
    parser.add_argument("--video-frames-per-second", type=int, default=20, help='only used on the video recording')
    parser.add_argument("--display-sleep-second", type=float, default=0.01, help='only used for display')
    parser.add_argument("--dic-variable-max-episode-len", type=str, default='{}')
    parser.add_argument("--seed", type=int, default=None, help="random seed")
    return parser.parse_args()



def make_env(scenario_name, arglist, benchmark=False):
    from multiagent.environment import MultiAgentEnv
    import multiagent.scenarios as scenarios

    # load scenario from script
    scenario = scenarios.load(scenario_name + ".py").Scenario()
    # create world
    world = scenario.make_world()
    # create multiagent environment
    if benchmark:
        env = MultiAgentEnv(world, scenario.reset_world, scenario.reward, scenario.observation, scenario.benchmark_data)
    else:
        env = MultiAgentEnv(world, scenario.reset_world, scenario.reward, scenario.observation)
    return env

def set_dirs(arglist):
    if arglist.display:
        return
    elif arglist.restore:
        _restore_dirs(arglist)
    else:
        _set_new_dirs(arglist)


def _restore_dirs(arglist):
    arglist.save_dir = osp.dirname(arglist.load_model)
    arglist.plots_dir = arglist.save_dir.replace('models', 'learning_curves')


def _set_new_dirs(arglist):
    exp_dir = './exp_results'
    if arglist.exp_name is None:
        arglist.exp_name = arglist.scenario + '__' + time.strftime("%Y-%m-%d_%H-%M-%S")
        if arglist.seed is not None:
            arglist.exp_name += '_seed%d' % arglist.seed
    exp_dir = osp.join(exp_dir, arglist.exp_name)

    if arglist.plots_dir is None:
        arglist.plots_dir = osp.join(exp_dir, 'learning_curves')
    if arglist.save_dir is None:
        arglist.save_dir = osp.join(exp_dir, 'models')

    for d in (arglist.plots_dir, arglist.save_dir):
        if not osp.exists(d):
            os.makedirs(d, exist_ok=True)



def set_random_seed(env, seed):
    import random
    if seed is None:
        return
    random.seed(seed)
    np.random.seed(seed)
    #tf.set_random_seed(seed)
    env.seed(seed)


arglist = parse_args()
set_dirs(arglist)

env = make_env(arglist.scenario, arglist, arglist.benchmark)
set_random_seed(env, arglist.seed)

observation = env.reset()

#パラメーター
NGEN = 1000#世代数
POP = 400#個体数
TRY = 5#試行回数
STEP = 40

CXPB = 0.5#交叉率
MUTPB = 0.3#突然変異率

num_state = env.observation_space[0].shape[0]
num_action = 5

neuron_num = [num_state,64,64,num_action]
net = [Net(neuron_num) for p in range(POP)]
best_net = Net(neuron_num)
[net[p].joint for p in range(POP)]

n_gene = len(net[0].ind)
min_ind = np.ones(n_gene) *  -1.0
max_ind = np.ones(n_gene) *  1.0
toolbox = base.Toolbox()
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("mate", deapNp.cxTwoPointCopy)
toolbox.register("mutate", deapNp.mutUniformDbl, min_ind=min_ind, max_ind=max_ind, indpb=MUTPB)

print(arglist.scenario)

#----------評価関数--------------------------------------------------------------
def eval_fitness():
    fitnesses =[]
    for p in range(POP): 
        F = 0.0
        for t in range(TRY):
            observation = env.reset()
            for s in range(STEP):
                actions = []
                episode_rewards = [0.0]  # sum of rewards for all agents
                agent_rewards = [[0.0] for _ in range(env.n)]  # individual agent reward
                for r in range(env.n):
                    obs = np.array([observation[r]],dtype=np.float32)
                    action = net[p](obs).data[0]
                    for i in range(len(action)):
                        if(action[i] >= 1.0):
                            action[i] = 1.0
                        if(action[i] <= -1.0):
                            action[i] = -1.0
                        action[i] = ((action[i] + 1) / 2.0)
                    actions.append(action)
                    #print(actions)
                next_observation, reward, done, info = env.step(actions)
                
                for i, rew in enumerate(reward):
                    episode_rewards[-1] += rew
                    agent_rewards[i][-1] += rew
                
                observation = next_observation
                F = F + np.mean(agent_rewards)
                if all(done):
                    break
        F = F/float(TRY)
        fitnesses.append((F,))
    return fitnesses
#------------------------------------------------------------------------------
new_dir_path= './exp_results_ga/'+arglist.scenario+str(dt_now.month)+'_'+str(dt_now.day)+'_'+str(dt_now.hour)+'_'+str(dt_now.minute)
os.mkdir(new_dir_path)

fitnesses = eval_fitness()
pop = [net[p].joint() for p in range(POP)]
for ind, fit in zip(pop, fitnesses):
    ind.fitness.values = fit

best_ind = tools.selBest(pop, 1)[0]
best_net.split(best_ind)

print("  Evaluated %i individuals" % len(pop))

if arglist.video_record:
    env.metadata['video.frames_per_second'] = arglist.video_frames_per_second
    recorder = gvr.VideoRecorder(env, arglist.video_file_name, enabled=True)
if arglist.exp_name is not None:
    print('Starting iterations of %s...' % arglist.exp_name)


#----------学習タスク-----------z--------------------------------------------------
for g in range(NGEN):
    print("-- Generation %i --" % g)
    #-----------選択--------------------------------
    #選択した個体を格納
    offspring = toolbox.select(pop, len(pop))
    
    #コピーして再度格納しなおす。fitnessとの参照を着るため？
    offspring = list(map(toolbox.clone, offspring))
    
    #--------交叉-----------------------------------------
    #offspringの偶数値をchild1奇数値をchild2とする。
    for child1, child2 in zip(offspring[::2], offspring[1::2]):
        if random.random() < CXPB:
            #ここで交叉、適応度を再計算するためにそれぞれの適応度を削除
            toolbox.mate(child1, child2)
            del child1.fitness.values
            del child2.fitness.values
    
    #--------突然変異--------------------------------------------------
    for mutant in offspring:
        if random.random() < MUTPB:
            toolbox.mutate(mutant)
            del mutant.fitness.values
    
    #-------適応度の再計算---------------------------------------------
    [net[p].split(offspring[p]) for p in range(POP)]
    pop = [net[p].joint() for p in range(POP)]
    fitnesses = eval_fitness()
    #print(fitnesses)
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit

    #-------状態確認-------------------
    fit_debug = [fitnesses[p][0] for p in range(POP)]
    
    max_rew = max(fit_debug)
    mean_rew = sum(fit_debug)/float(len(fit_debug))
    rew_file_name = osp.join(new_dir_path, 'reward.csv')
    
    is_first_save = False
    if not osp.exists(rew_file_name):
        is_first_save = True
    with open(rew_file_name, 'a') as ex:
        if is_first_save:
            ex.write('episode,mean_reward, max_reward\n')
        ex.write('%d, %f, %f\n' % (g, mean_rew, max_rew))
          
    best_ind = tools.selBest(pop, 1)[0]
    best_net.split(best_ind)
    npy_name = str(g)
    npy_name = osp.join(new_dir_path, npy_name)
    best_net.save_npy(npy_name)
    print(max_rew)
    print(min(fit_debug))
    print(mean_rew)
          
    
print("-- End of (successful) evolution --")

#-------------最適個体のネットワークを保存---------------------------------------------

best_ind = tools.selBest(pop, 1)[0]
best_net.split(best_ind)
best_net.save_npy(new_dir_path)
env.close()
