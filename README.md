# Experiment with Multi-Agent Particle Environments

I have improved the experimental enviroment of MADDPG (https://github.com/openai/multiagent-particle-envs) and (https://github.com/openai/maddpg) described in the paper(https://arxiv.org/pdf/1706.02275.pdf).

![0125_result](/uploads/87403d620f9516a70e9dc74d161ab7c2/0125_result.jpg)

Improvements include the addition of a transport target that gives a negative reward in the event of a collision.
In addition, each robot is equipped with a LiDER so that the robot can recognize the transfer target from the robot's perspective.

The scenarios I added for my experiment is "simple_spread_avoid".